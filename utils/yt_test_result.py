from unittest import TestResult

import sys
import logging


class YtTestResult(TestResult):

    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger(__name__)

    def __init__(self, stream=None, descriptions=None, verbosity=None):
        super(YtTestResult, self).__init__(stream, descriptions, verbosity)
        self.failfast = False
        self.failures = []
        self.errors = []
        self.testsRun = 0
        self.skipped = []
        self.expectedFailures = []
        self.unexpectedSuccesses = []
        self.shouldStop = False
        self.buffer = False
        self._stdout_buffer = None
        self._stderr_buffer = None
        self._original_stdout = sys.stdout
        self._original_stderr = sys.stderr
        self._mirrorOutput = False

        self.results = list()

    def startTest(self, test):
        print("="*60)
        print("Testing: {0}\n".format(str(test)))

    def addSuccess(self, test):
        self.results.append(dict(test_case=test.id(), status='SUCCESS'))
        # self.log.info("SUCCESS!")
        print("SUCCESS!\n")

    def addError(self, test, err):
        self.results.append(dict(test_case=test.id(), status='FAIL'))
        self.errors.append((test, self._exc_info_to_string(err, test)))
        self._mirrorOutput = True
        # self.log.error(err)
        print("ERROR: \n {0} \n".format(err))

    def addFailure(self, test, err):
        self.results.append(dict(test_case=test.id(), status='FAIL'))
        self.failures.append((test, self._exc_info_to_string(err, test)))
        self._mirrorOutput = True
        # self.log.error(err)
        print("FAILURE: \n {0} \n".format(err))
