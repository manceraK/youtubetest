from __future__ import print_function
import csv
import json

def read_csv_dict(filename):
    with open(filename, 'r') as file:
        data = []
        reader = csv.DictReader(file, skipinitialspace=True)
        for line in reader:
            data.append(line)
        return data


def save_dict_csv(filename, data, fieldnames, path=None):
    with open(filename, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        writer.writerows(data)

def save_json(filename, data):
    with open(filename, 'w') as outfile:
        json.dump(data, outfile, indent = 2)
