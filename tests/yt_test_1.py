from __future__ import print_function

import unittest

from selenium import webdriver
from pages.yt_home_page import YoutubeHomePage
from pages.yt_video_page import YoutubeVideoPage
from utils import file_utils


class YoutubeTestCase1(unittest.TestCase):

    def setUp(self):
        # self.browser = webdriver.Chrome('vendor/chromedriver')
        self.browser = webdriver.Chrome()
        self.addCleanup(self.browser.quit)

    def testVideoSearch(self):
        csv_test_data_path = 'search_test_data.csv'
        data_to_test = file_utils.read_csv_dict(csv_test_data_path)
        yt_home_url = data_to_test[0].get('url')
        yt_search_term = data_to_test[0].get('search_term')

        yt_home = YoutubeHomePage(self.browser, yt_home_url)
        yt_home.open()

        video = yt_home.search(yt_search_term)

        self.assertIsNotNone(video, "Video '{0}' not found".format(yt_search_term))

    def testVideoPlayback(self):
        csv_test_data_path = 'video_playback_test_data.csv'
        data_to_test = file_utils.read_csv_dict(csv_test_data_path)
        video_url = data_to_test[0].get('url')

        yt_video = YoutubeVideoPage(self.browser, video_url)
        yt_video.open()

        yt_video.play_video()
        self.assertTrue(yt_video.is_video_playing())

        yt_video.pause_video()
        self.assertTrue(yt_video.is_video_paused())

        yt_video.play_video()
        self.assertTrue(yt_video.is_video_playing())

        volume_before = yt_video.get_volume()
        yt_video.turn_volume_down()
        current_volume = yt_video.get_volume()
        self.assertLess(current_volume, volume_before)

        volume_before = yt_video.get_volume()
        yt_video.turn_volume_down()
        current_volume = yt_video.get_volume()
        self.assertLess(current_volume, volume_before)

        volume_before = yt_video.get_volume()
        yt_video.turn_volume_up()
        current_volume = yt_video.get_volume()
        self.assertGreater(current_volume, volume_before)

        time_before = yt_video.get_current_time()
        yt_video.move_time_slider(300)
        current_time = yt_video.get_current_time()
        self.assertNotEqual(time_before, current_time)

        time_before = yt_video.get_current_time()
        yt_video.move_time_slider(-80)
        current_time = yt_video.get_current_time()
        self.assertNotEqual(time_before, current_time)

        time_before = yt_video.get_current_time()
        yt_video.move_time_slider(-50)
        current_time = yt_video.get_current_time()
        self.assertNotEqual(time_before, current_time)

if __name__ == '__main__':
    t = unittest.main(verbosity=2)

