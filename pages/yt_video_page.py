import logging
from selenium import webdriver
from pages.page import PageObject
from selenium.common.exceptions import NoSuchElementException


class YoutubeVideoPage(PageObject):

    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger(__name__)

    def open(self):
        self.browser.get(self.url)
        self.wait_for_video_available()


    def play_video(self):
        # move_to_element need to focus the ytp-chrome-controls first?
        if self.is_video_playing():
            return

        play_button = self.browser.find_element_by_class_name('ytp-play-button')
        play_button.click()
        self.log.info("Video Playing")
        self.sleep(1)

    def pause_video(self):
        if self.is_video_paused():
            return
        pause_button = self.browser.find_element_by_class_name('ytp-play-button')
        pause_button.click()
        self.log.info("Video Paused")
        self.sleep(1)

    def turn_volume_up(self):
        step = 5
        volume_delta = 0

        volume_panel = self.browser.find_element_by_class_name('ytp-mute-button')
        volume_slider = self.browser.find_element_by_class_name('ytp-volume-slider-handle')
        vol_x_pos = int(float(volume_slider.value_of_css_property('left')[:-2]))

        if vol_x_pos + step > 40:
            volume_delta = 40 - vol_x_pos
        else:
            volume_delta = step

        volume_slider_action = webdriver.ActionChains(self.browser)
        volume_slider_action.move_to_element(volume_panel).perform()
        self.wait_for_visible(volume_slider)
        volume_slider_action.drag_and_drop_by_offset(volume_slider, volume_delta, 0).perform()
        self.log.info("Volume turned up")
        self.sleep(1)

    def turn_volume_down(self):
        step = 5
        volume_delta = 0

        volume_panel = self.browser.find_element_by_class_name('ytp-mute-button')
        volume_slider = self.browser.find_element_by_class_name('ytp-volume-slider-handle')
        vol_x_pos = int(float(volume_slider.value_of_css_property('left')[:-2]))

        if vol_x_pos < step:
            volume_delta = -vol_x_pos
        else:
            volume_delta = -step

        volume_slider_action = webdriver.ActionChains(self.browser)
        volume_slider_action.move_to_element(volume_panel).perform()
        self.wait_for_visible(volume_slider)
        volume_slider_action.drag_and_drop_by_offset(volume_slider, volume_delta, 0).perform()
        self.log.info("Volume turned down")
        self.sleep(1)

    def move_time_slider(self, time_offset):
        progress_bar_cont = self.browser.find_element_by_class_name('ytp-progress-bar-container')
        time_slider = self.browser.find_element_by_class_name('ytp-scrubber-button')
        time_slider_action = webdriver.ActionChains(self.browser)
        time_slider_action.move_to_element(progress_bar_cont).perform()
        self.wait_for_visible(progress_bar_cont)
        time_slider_action.move_to_element(time_slider).drag_and_drop_by_offset(time_slider, time_offset, 0).perform()
        self.log.info("Moving the time slider")
        self.sleep(2)

    def get_player_state(self):
        # 0 -> stop, 1 -> playing, 2 -> paused
        player_state = self.browser.execute_script("return document.getElementById('movie_player').getPlayerState();")
        return player_state

    def is_video_playing(self):
        # We can also know if the video is playing by seeing if the the div with the id #movie_player has the class 'playing-mode'.
        is_playing = self.get_player_state() == 1
        return is_playing

    def is_video_paused(self):
        # We can also know if the video is playing by seeing if the the div with the id #movie_player has the class 'pause-mode'.
        is_paused = self.get_player_state() == 2
        return is_paused

    def get_volume(self):
        # Another way of getting the volume is from the aria-value attribute of the div with the class 'ytp-volume-panel'.
        volume = self.browser.execute_script("return document.getElementById('movie_player').getVolume();")
        return volume

    def get_current_time(self):
        # Another way of getting the current time is from the 'aria-valuenow' attribute of the div with the class 'ytp-progress-bar'
        # the attributes 'aria-valuemin' and 'aria-valuemax' could also be useful.
        return self.browser.execute_script("return document.getElementById('movie_player').getCurrentTime();")

    def wait_for_video_available(self):
        video_timeout_interval = 50
        video_loaded = False
        for i in range(video_timeout_interval):
            is_available = False
            try:
                player = self.browser.find_element_by_class_name('html5-video-player')
                video_loaded = 'unstarted-mode' not in player.get_attribute('class')
                is_available = True
            except NoSuchElementException:
                is_available = False

            if is_available:
                if video_loaded:
                    break
            self.sleep()
        else:
            raise Exception("Wait for video timed out.")
        self.log.info("Video ready.")
        # self.sleep(2)
        return True
