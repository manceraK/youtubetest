import logging
from pages.page import PageObject
from selenium.webdriver.common.by import By


class YoutubeHomePage(PageObject):

    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger(__name__)

    def open(self):
        self.browser.get(self.url)
        self.wait_for_page_load()

    def search(self, query):
        self.search_text = query
        search_box = self.browser.find_element_by_id('masthead-search-term')
        search_box.clear()
        search_box.send_keys(query)
        search_box.submit()
        self.log.info('Searching "{0}"...'.format(query))
        self.wait_for_available(By.ID, 'results')
        self.wait_for_available(By.CLASS_NAME, 'yt-uix-tile-link')
        search_results = self.browser.find_elements_by_class_name('yt-uix-tile-link')
        video_found = next((x for x in search_results if x.text == query), None)

        return video_found

    def wait_for_page_load(self):
        self.wait_for_available(By.ID, 'masthead-search-term', 60, "Wait for Youtube home page to load timed out.")
        self.log.info("Youtube home page loaded.")
