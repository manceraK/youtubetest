import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import Remote
from selenium.webdriver.common.by import By


class PageObject(object):
    timeout_interval = 20  # timeout --> seconds = timeout_interval * sleep_interval
    sleep_interval = .25

    def __init__(self, browser, url=None):

        """
        Class constructor

        :type url: String
        :type browser: Remote
        """
        self.browser = browser
        self.url = url

    def open(self):
        self.browser.get(self.url)

    def close(self):
        self.browser.close()

    def sleep(self, seconds=None):
        if seconds:
            time.sleep(seconds)
        else:
            time.sleep(self.sleep_interval)

    def wait_for_visible(self, web_element):
        """
        Waits for the given element to be visible.
        :param web_element:
        :return:
        """

        for i in range(self.timeout_interval):
            if web_element.is_displayed():
                break
            self.sleep()
        else:
            raise Exception("Wait for available timed out.")
        return True

    def wait_for_selected(self, web_element):
        """
        Waits for the given element to be selected.
        :param web_element:
        :return:
        """
        for i in range(self.timeout_interval):
            if web_element.is_selected():
                break
            self.sleep()
        else:
            raise Exception("Wait for selected timed out.")
        return True

    def wait_for_available(self, By, selector, timeout=None, exception_text=None):
        # type: (By, String, Number, Boolean) -> Boolean

        """
        Waits for the given element to be available on the DOM. If the element is not found in the given interval
        an exception will be raised.

        :param By: By
        :param selector: String
        :param timeout: Number
        :param exception_text: Boolean
        :return: Boolean
        """
        if timeout is None:
            timeout = 60  # seconds = timeout * self.sleep_interval
        for i in range(timeout):
            try:
                self.browser.find_element(By, selector)
                is_available = True
            except NoSuchElementException:
                is_available = False

            if is_available:
                break
            self.sleep()
        else:
            if exception_text is None:
                exception_text = "Wait for {0} '{1}' to be available timed out.".format(By, selector)
            raise Exception(exception_text)
        return True
