from unittest import TestLoader, TestSuite, TextTestRunner
from utils.yt_test_result import YtTestResult
from utils import file_utils

from tests import yt_test_1, yt_test_2, yt_test_3, yt_test_4, yt_test_5

if __name__ == "__main__":
    loader = TestLoader()
    suite = TestSuite()
    suite.addTest(yt_test_1.YoutubeTestCase1('testVideoSearch'))
    suite.addTest(yt_test_1.YoutubeTestCase1('testVideoPlayback'))
    suite.addTest(yt_test_2.YoutubeTestCase2('testVideoSearch'))
    suite.addTest(yt_test_2.YoutubeTestCase2('testVideoPlayback'))
    suite.addTest(yt_test_3.YoutubeTestCase3('testVideoSearch'))
    suite.addTest(yt_test_3.YoutubeTestCase3('testVideoPlayback'))
    suite.addTest(yt_test_4.YoutubeTestCase4('testVideoSearch'))
    suite.addTest(yt_test_4.YoutubeTestCase4('testVideoPlayback'))
    suite.addTest(yt_test_5.YoutubeTestCase5('testVideoSearch'))
    suite.addTest(yt_test_5.YoutubeTestCase5('testVideoPlayback'))

    # suite = TestSuite((
    #     loader.loadTestsFromTestCase(yt_test_1.YoutubeTestCase1),
    #     loader.loadTestsFromTestCase(yt_test_2.YoutubeTestCase2),
    #     loader.loadTestsFromTestCase(yt_test_3.YoutubeTestCase3),
    #     loader.loadTestsFromTestCase(yt_test_4.YoutubeTestCase4),
    #     loader.loadTestsFromTestCase(yt_test_5.YoutubeTestCase5),
    # ))

    runner = TextTestRunner(verbosity=2, resultclass=YtTestResult).run(suite)

    file_utils.save_dict_csv('test_results.csv', runner.results, ['test_case', 'status'])
    file_utils.save_json('test_results.json', runner.results)

    print(runner)