# README #

Requires python 2.7 (tested on a linux enviroment).

### Instructions ###

1.- Create a new virtualenv (You need to be on the same folder as this README):


```
#!bash

$ virtualenv env
```

2.- activate the virtualenv:


```
#!bash

$ source env/bin/activate
```

3.- Install the dependencies:


```
#!bash

pip install -r requeriments.txt
```

4.- Run the script:


```
#!bash

$ python youtube_test_suite.py
```

Results are saved to the test_results.csv and test_results.json files.
